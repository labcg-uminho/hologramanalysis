#!/bin/sh

if [ "$1" = "mkl" ] || [ "$1" = "ipp" ]; then

    case $1 in "mkl") REF=12070;; "ipp") REF=12071;; esac

    DL_CMD=""
    if [ -x "$(command -v wget)" ]; then
        DL_CMD="wget -O -"
    elif [ -x "$(command -v curl)" ]; then
        DL_CMD="curl -L"
    else
        echo 'ERROR: neither wget nor curl is installed.' >&2
        exit 1
    fi

    echo -n "Do you want to download and install the $1 library now? [Y/n] "
    read -r response
    if echo "$response" | grep -iq "^y$" || [ -z "$response" ]; then
        cd /tmp &&
        $DL_CMD "http://registrationcenter-download.intel.com/akdlm/irc_nas/tec/$REF/l_$1_2018.0.128.tgz" | tar -xz &&
        cd "l_$1_"* &&
        perl -p -i -e 's/ACCEPT_EULA=decline/ACCEPT_EULA=accept/' silent.cfg &&
        sudo ./install.sh --silent ./silent.cfg
    else
        return 1
    fi
else
    echo "ERROR: This script must be called with the parameter 'mkl' or 'ipp'." >&2
    return 1
fi