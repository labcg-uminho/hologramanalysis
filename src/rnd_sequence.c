#include "rnd_sequence.h"
#include "ipp.h"
#include "mkl_service.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// A utility function to swap two elements
static void swap(int *a, int *b) {
  int t = *a;
  *a = *b;
  *b = t;
}

// generates a random sequence of k integers, lying in the interval (0 .. n-1)

int rnd_sequence(int *r, int n, int k) {
  VSLStreamStatePtr stream;
  int *ibuf = 0;
  double *dbuf = 0;
  int i;
  char str_aux[100];

  ibuf = (int *)mkl_malloc(n * sizeof(int), 64);
  if (ibuf == 0) {
    fprintf(stderr, "rnd_sequence(): could not alloc memory (%d * sizeof(int)).\n", n);
    return 0;
  }

  dbuf = (double *)mkl_malloc(k * sizeof(double), 64);
  if (dbuf == 0) {
	  fprintf(stderr, "rnd_sequence(): could not alloc memory (%d * sizeof(int)).\n", n);
	  return 0;
  }

  /* Initializing */
  vslNewStream(&stream, VSL_BRNG_SFMT19937, 777);

  /* Generating k doubles between 0 and 1*/
  vdRngUniform(VSL_RNG_METHOD_UNIFORM_STD, stream, k, dbuf, 0.0, 0.999);

  /* Deleting the stream */
  vslDeleteStream(&stream);

  // Fill in ibuf
  for (i = 0; i < n; i++) ibuf[i] = i;

  // shuffle ibuf first k elements
  int ndx;
  for (i = 0; i < k; i++) {
    ndx = (int)floor((dbuf[i] * (double)(n)));
    ndx = (ndx == n ? n - 1 : ndx);
    swap(&ibuf[i], &ibuf[ndx]);
  }
  // copy k first integers from *ibuf to *r
  memcpy((void *)r, (void *)ibuf, k * sizeof(int));

  mkl_free(dbuf);
  mkl_free(ibuf);

  return 1;
}

// generates a random sorted sequence of k integers, lying in the interval (0 .. n-1)

int sorted_rnd_sequence(int *r, int n, int k) {
  IppStatus status;

  if (!rnd_sequence(r, n, k)) return 0;

  // sort r
  status = ippsSortAscend_32s_I(r, k);
  if (status != ippStsNoErr) {
    fprintf (stderr, "sorted_rnd_sequence(): ippsSortAscend failed to sort.\n");
    return 0;
  }
  return 1;
}
