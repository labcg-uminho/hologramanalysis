#include <stdio.h>
#include <string.h>
#include <mkl.h>

// Force MSVC to accept fopen()
#ifdef _MSC_VER
	#pragma warning(disable:4996)
#endif

int holoread(char *holofileName, MKL_Complex8 **signal_full) {
	FILE *f;
	int dims[2], N, c;

	f = fopen(holofileName, "rb");
	if (f == NULL) {
		fprintf(stderr, "holoread(): could not open file «%s».\n", holofileName);
		return 0;
	}

	if (fread(dims, sizeof(int), 2, f) != 2) {
		fprintf(stderr, "holoread(): could not read hologram dimensions.\n");
		fclose(f);
		return 0;
	}
	N = dims[0] * dims[1];

	*signal_full = (MKL_Complex8 *)mkl_malloc(N*sizeof(MKL_Complex8), 64);
	if ((*signal_full) == NULL) {
		fprintf(stderr, "holoread(): could not alloc mem for the full hologram signal.\n");
		fclose (f);
		return 0;
	}

	c = 0;
	while (c < N && !feof(f)) {
		double buff;
		if (fread(&buff, sizeof(double), 1, f) != 1) {
			fprintf(stderr, "holoread(): could not read hologram data (c=%d).\n", c);
			fclose(f);
			return 0;
		}
		else {
			(*signal_full)[c].real = (float)buff;
			(*signal_full)[c].imag = 0.;
			c++;
		}
	}

	fclose(f);
	return N;
}


// save recovered data, for eventual display by the MatLab script
int save_exp_data (char *holofileName, int N, int m, int k, char * alg, int *support, MKL_Complex8 *nonZeroCoeffs) {
	FILE *save_data;
	char outfileName[150], *aux;
	int ret;

	strcpy(outfileName, holofileName);
	// cut eventual filename extension
	aux = strrchr(outfileName, '.');
	if (aux) {  // '.' found at the right of the filename
		*aux = '\0';
	}
	// build filename
	sprintf(outfileName, "%s_%s_N%d_m%d_k%d", outfileName, alg, N, m, k);

	save_data = fopen(outfileName, "wb");
	ret = (save_data != NULL);
	if (ret) {
		int iaux;

		fwrite((void *)&N, sizeof(int), 1, save_data);
		fwrite((void *)&m, sizeof(int), 1, save_data);
		fwrite((void *)&k, sizeof(int), 1, save_data);
		iaux = strlen(holofileName);
		fwrite((void *)&iaux, sizeof(int), 1, save_data);
		fwrite((void *)holofileName, sizeof(char), iaux + 1, save_data);
		iaux = strlen(alg);
		fwrite((void *)&iaux, sizeof(int), 1, save_data);
		fwrite((void *)alg, sizeof(char), iaux + 1, save_data);
		// support
		fwrite((void *)support, sizeof(int), k, save_data);
		// nonZeroCoeffs real part
		for (iaux = 0; iaux < k; iaux++) {
			fwrite((void *)&(nonZeroCoeffs[iaux].real), sizeof(float), 1, save_data);
		}
		// nonZeroCoeffs imaginary part
		for (iaux = 0; iaux < k; iaux++) {
			fwrite((void *)&(nonZeroCoeffs[iaux].imag), sizeof(float), 1, save_data);
		}
		fclose(save_data);
		fprintf(stdout, "Data saved on %s\nWithin MatLab write\n> HologramAnalysis_plot (%s)\n\n", outfileName, outfileName);
	}

	return ret;
}
