#ifndef __COSAMP_H_
#define __COSAMP_H_

#include "mkl.h"

// maxc - nbr of coefficients to retrieve (usually refrerred to as k)
// samples - array of indexes of sampled points on the space domain
// y - array of the values of the signal on the points indexed by samples
// vOut - output signal on the sparse representation basis (n elements, maxc != 0)
// n - signal's ambient dimensionality
// m - nbr of measurements (#elements on samples and y)

int CoSaMP (int maxc, int *samples, MKL_Complex8 *y, int *support, MKL_Complex8 *coeffs, int n, int m);

// maxc - nbr of coefficients to retrieve (usually refrerred to as k)
// A - m rows * n columns compressive measurement matrix 
// y - array of the signal's measured values
// coeffs - output of the maxc non-zero coefficients' values (if coeff==NULL their values are not computed) 
// support - output of the indexes of the maxc non-zero coefficients
// n - signal's ambient dimensionality
// m - nbr of measurements (#rows of A, #elements on y)
int CoSaMP_supp(int maxc, MKL_Complex8 *y, MKL_Complex8 *coeffs, int *support, int N, int n_cols, int *cols, int m, int *samples);

#endif
