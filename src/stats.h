#pragma once

#include <mkl.h> 

int recovery_stats(int N, int m, int k, int *support, MKL_Complex8 *nonZeroCoeffs, MKL_Complex8 *signal_full);
