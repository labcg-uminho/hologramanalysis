
#ifdef _MSC_VER
	#define _CRT_SECURE_NO_WARNINGS
#endif

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mkl.h>

/*
 * Computing the measurement matrix (dftmtx)
 */
#include "dftmtx.h"

/* Sorted random integer sequences between 0 and m-1 */
#include "rnd_sequence.h"

/* Read hologram files */
#include "holofile.h"

/* evaluate and poutput some stats */
#include "stats.h"

/* omp */
#include "omp.h"
/* CoSaMP */
#include "CoSaMP.h"

#define MAX_EXP_N 24
#define MAX_EXP_M MAX_EXP_N
#define MAX_EXP_K (MAX_EXP_N-2)

typedef enum { OMP = 0, COSAMP = 1} ALGS;

char *algs[] = { "OMP", "COSAMP"};
char *commandName = "HologramAnalysis";

static void usage();
static int get_cmd_line(int argc, char *argv[], int *m, int *k, ALGS *a, char *holofileName);
static int my_is_integer(char *s);
static void alloc_mem(int m, int k, MKL_Complex8 **signal_m, int **samples, int **support, MKL_Complex8 **nonZeroCoeffs);

FILE *out_stream;

int main(int argc, char *argv[]) {

	commandName = argv[0];

	/* Size of 1D transform */
	int N = 0, m = 0, k = 0, i = 0;

	ALGS alg = COSAMP;
	MKL_Complex8 *signal_full = 0, *signal_m = 0, *nonZeroCoeffs = 0;
	int *support = 0, *samples = 0, numIts = 0;
	char holofileName[120];

	out_stream = stdout;

	// if some control over MKL number of used threads is desired then uncomment and parameterize below
	// see: https://software.intel.com/en-us/mkl-developer-reference-c-2018-beta-threading-control
	// mkl_set_num_threads(1);

	if (get_cmd_line(argc, argv, &m, &k, &alg, holofileName) <= 1) {
		fprintf(stderr, "[FAIL] main(): could not process command line.\n");
		exit(1);
	}

	N = holoread(holofileName, &signal_full);
	if (!N) {
		fprintf(stderr, "[FAIL] main(): error reading hologram information.\n");
		exit(1);
	}

	if (N > (1 << MAX_EXP_N)) {
		fprintf(stderr, "[FAIL] main(): N can't be larger than pow2(MAX_EXP_N).\n");
		exit(1);
	}
	if (m > (1 << MAX_EXP_M)) {
		fprintf(stderr, "[WARN] main(): limiting m to the maximum allowed value (2^%d).\n", MAX_EXP_M);
		m = 1 << MAX_EXP_M;
	}
	if (m > N) {
		fprintf(stderr, "[WARN] main(): m (%d) can't be larger than N (%d); auto-correcting.\n", m, N);
		m = N;
	}
	if (k > (1 << MAX_EXP_K)) {
		fprintf(stderr, "[WARN] main(): limiting k to the maximum allowed value (2^%d).\n", MAX_EXP_K);
		k = 1 << MAX_EXP_K;
	}

	fprintf(out_stream, "[INFO] Parameters: N=%d, m=%d, k=%d, alg=%s\n", N, m, k, algs[alg]);

	alloc_mem(m, k, &signal_m, &samples, &support, &nonZeroCoeffs);

	sorted_rnd_sequence(samples, N, m);

	// generate the sampled signal
	for (i = 0; i < m; i++) {
		signal_m[i].real = signal_full[samples[i]].real;
		signal_m[i].imag = signal_full[samples[i]].imag;
	}

	switch (alg) {
		case OMP:
			if (!(numIts = omp(k, samples, signal_m, support, nonZeroCoeffs, N, m))) {
				fprintf(stderr, "[FAIL] main(): OMP() failed.\n");
				exit(1);
			}
			break;
		case COSAMP:
		default:
			if (m < 3 * k) {
				fprintf(stderr, "[FAIL] main(): CoSaMP requires m to be >= 3*k.\n");
				exit(1);
			}
			if (!(numIts = CoSaMP(k, samples, signal_m, support, nonZeroCoeffs, N, m))) {
				fprintf(stderr, "[FAIL] main(): CoSaMP() failed.\n");
				exit(1);
			}
			break;
	} // switch (alg)

	// if mem is available (N size vectors) use:
	recovery_stats(N, m, k, support, nonZeroCoeffs, signal_full);

	// save recovered data, for eventual display by the MatLab script
	save_exp_data(holofileName, N, m, k, algs[alg], support, nonZeroCoeffs);

	mkl_free(samples);
	mkl_free(signal_m);
	mkl_free(signal_full);
	mkl_free(support);
	mkl_free(nonZeroCoeffs);

	return 1;
}

static void usage() {
	printf("\nUsage:\n\t%s -holo <hologram-filename> -m <num-measurements> -k <nonzero-coeffs> [-alg <cs-algorithm>]\n\n", commandName);
}

static int get_cmd_line(int argc, char *argv[], int *m, int *k, ALGS *alg, char *holofileName) {
	int count = 1, exp;

	if (argc == 1) {
		fprintf(stderr, "[FAIL] get_cmd_line(): no command line arguments were passed.\n");
	}
	while (count < argc) {
		if (!strcmp(argv[count], "-m")) { // nbr of measurements
			count++;
			if ((count < argc) && my_is_integer(argv[count])) {
				exp = atoi(argv[count]);
				*m = 1 << exp;
				count++;
			} else {
				fprintf(stderr, "[FAIL] get_cmd_line(): did not find integer argument for -m.\n");
				usage();
				exit(1);
			}
		} else if (!strcmp(argv[count], "-k")) { // signal_mp sparsity
			count++;
			if ((count < argc) && my_is_integer(argv[count])) {
				exp = atoi(argv[count]);
				*k = 1 << exp;
				count++;
			} else {
				fprintf(stderr, "[FAIL] get_cmd_line(): did not find integer argument for -k.\n");
				usage();
				exit(1);
			}
		} else if (!strcmp(argv[count], "-alg")) { // algorithm
			char *t;
			count++;
			if (count < argc) {
				for (t = argv[count]; *t; t++) *t = toupper(*t); // uppercase
				if (!strcmp(argv[count], algs[OMP])) {
					*alg = OMP;
				}
				else if (!strcmp(argv[count], algs[COSAMP])) {
					*alg = COSAMP;
				}
				else {
					fprintf(stderr,"[FAIL] get_cmd_line(): did not find known string argument for -alg.\n");
					usage();
					exit(1);
				}
				count++;
			} else {
				fprintf(stderr, "[FAIL] get_cmd_line(): did not find string argument for -alg.");
				usage();
				exit(1);
			}
		}
		else if (!strcmp(argv[count], "-holo")) { // hologram filename
			count++;
			if (count < argc) {
				strcpy(holofileName, argv[count]);
				count++;
			}
			else {
				fprintf(stderr, "[FAIL] get_cmd_line(): did not find string argument for -holo.");
				usage();
				exit(1);
			}
		} else { // unknown parameter
			fprintf(stderr, "[WARN] get_cmd_line(): unknown command line argument; skipping.\n");
			count++;
		}
	}
	return count;
}

static int my_is_integer(char *s) {
	while (*s) {
		if (!isdigit(*s))
			return 0;
		else
			s++;
	}
	return 1;
}

static void alloc_mem(
	int m,
	int k,
	MKL_Complex8 **signal_m,
	int **samples,
	int **support,
	MKL_Complex8 **nonZeroCoeffs
) {

	*samples = (int *)mkl_malloc(m * sizeof(int), 64);
	if (0 == *samples) {
		fprintf(stderr, "[FAIL] main(): could not malloc samples.\n");
		exit(1);
	}

	*signal_m = (MKL_Complex8 *)mkl_malloc(m * sizeof(MKL_Complex8), 64);
	if (0 == *signal_m) {
		fprintf(stderr, "[FAIL] main(): could not malloc the measured signal vector.\n");
		exit(1);
	}

	*support = (int *)mkl_malloc(k * sizeof(int), 64);
	if (support == NULL) {
		fprintf(stderr, "[FAIL] main(): could not malloc support.\n");
		exit(1);
	}

	*nonZeroCoeffs = (MKL_Complex8 *)mkl_malloc(k * sizeof(MKL_Complex8), 64);
	if (0 == *nonZeroCoeffs) {
		fprintf(stderr, "[FAIL] main(): could not malloc the non zero coeffs vector.\n");
		exit(1);
	}
}
