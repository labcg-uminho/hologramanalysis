#pragma once

/* The C interface provides the DFTI_DESCRIPTOR_HANDLE type,
* named constants of two enumeration types DFTI_CONFIG_PARAM and DFTI_CONFIG_VALUE,
*  and functions, some of which accept different numbers of input arguments.
*/
#include "mkl.h"
#include "mkl_dfti.h"
#include "mkl_service.h"

#define MY_PI 3.141592653589793
#define MY_PIf 3.141592653589793f

/* idftmtx is equivalent to its MatLab homonymous
* it will return in *A the inverse dft transformation matrix for:
* n dimensional signals, meaning the matrix will have n columns
* m points in the signal_mp space doimain, meaning the matrix will have m rows
* the indices of these points (i.e., which fourier inverse basis functions to evaluate)
* are given in *ndx, a m-element vector, with each element ranging from 0 to n-1
* note that the storage space in *A = m*n*sizeof(MKL_Complex8) must be previously allocated
*/

int idftmtx (MKL_Complex8 *A, int *ndx, int n, int m);

int idft_atoms(int N, int n_rows, int *rows, int n_cols, int *cols, int n_results, MKL_Complex8 *exponents, MKL_Complex8 *results);
