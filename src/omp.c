#include <string.h>
#include <stdio.h>
#include <math.h>
#include "omp.h"
#include "dftmtx.h"

int omp(int maxc, int *samples, MKL_Complex8 *y, int *support, MKL_Complex8 *coeffs, int n, int m)
{
    int numIts;
	MKL_Complex8 *A=0;

	numIts = omp_supp (maxc, y, coeffs, support, n, m, samples);

	return numIts;
}


int omp_supp (int maxc, MKL_Complex8 *y, MKL_Complex8 *valuesOut, int *support, int n, int m, int *samples)
{
	int i, coeffs, numIts, cpos, *cols = 0;
	MKL_Complex8 alpha = { 1.,0. }, beta = { 0.,0. }, *dots = 0, *r = 0;
	MKL_Complex8 *ASubI = 0, *LSQ = 0, *exps_buffer = 0, *outcore_A = 0;

	const int Nsub = maxc, Nreps = n / Nsub;  // how many times to iterate
	float local_max_abs, max_abs;
	int reps, cs, colBase, lcpos; // lcpos - local cpos

	//  Notes on not incore execution path
	//  - not incore means that the whole m rows * N columns A matrix is not precomputed and stored
	//  - but the matrix product A' * r must still be perfomed
	//  - it is done by processing subsets of m rows * N_sub columns of A
	//  - the smaller N_sub the less memory is used, 
	//    BUT the more iterations are required by the residual projection stage

	// dots: Nsub*1 vector to store the Nsub dot products between A_transpose and the residual
	dots = (MKL_Complex8 *)mkl_malloc(Nsub*sizeof(MKL_Complex8), 64);
	if (dots == 0) {
		fprintf(stderr , "omp: could not alloc memory for storing the dot products\n");
		return 0;
	}
	// r: m*1 vector to store the residual
	r = (MKL_Complex8 *)mkl_malloc(m*sizeof(MKL_Complex8), 64);
	if (r == 0) {
		fprintf(stderr , "omp: could not alloc memory for storing the residual\n");
		return 0;
	}
	// LSQ: m*1  vector to store the least squares (LSQ) result
	// (note that m elements are required -- even though there are at most 2*maxc coefficients recovered
	// this is because LSQ is also the input buffer for the LSQ routine
	// and because this routine returns additional info on the remaining elements of LSQ ...
	LSQ = (MKL_Complex8 *)mkl_malloc(m*sizeof(MKL_Complex8), 64);
	if (LSQ == 0) {
		fprintf(stderr , "omp: could not alloc memory for storing LSQ results\n");
		return 0;
	}
	// ASubI: m rows x maxc columns matrix with the selected basis functions
	ASubI = (MKL_Complex8 *)mkl_malloc(m*maxc*sizeof(MKL_Complex8), 64);
	if (ASubI == 0) {
		fprintf(stderr , "omp: could not alloc memory for storing the selected basis functions\n");
		return 0;
	}
	// cols - vector to hold the Nsub column indexes being handled at a particular iteration
	cols = (int *)mkl_malloc(Nsub*sizeof(int), 64);
	if (cols == 0) {
		fprintf(stderr , "omp: could not alloc memory for storing the out of core col IDs\n");
		return 0;
	}
	// exps buffer - buffer vector to the exponents within idft_atoms
	exps_buffer = (MKL_Complex8 *)mkl_malloc(Nsub*m*sizeof(MKL_Complex8), 64);
	if (exps_buffer == 0) {
		fprintf(stderr , "omp: could not alloc memory for storing exps_buffer for idft_atoms\n");
		return 0;
	}
	// incore_A used within the outcore version to hold the NSub columns (m rows each) 
	outcore_A = (MKL_Complex8 *)mkl_malloc(m*Nsub*sizeof(MKL_Complex8), 64);
	if (outcore_A == 0) {
		fprintf(stderr , "omp: could not alloc memory for storing the (outcore) selected basis functions\n");
		return 0;
	}

	coeffs = numIts = 0;
	// initialize residual with the signal_mp
	memcpy(r, y, m*sizeof(MKL_Complex8));

	// for computing the normalized length of the residual
	const float Inv_sqr_m = 1.f / sqrtf((float)m);

	while ((coeffs < maxc) && ((cblas_scnrm2(m, r, 1) * Inv_sqr_m) > 1.e-4)) {

		max_abs = -1.f;  // initialize max!!
		for (colBase = 0, reps = 0; reps < Nreps; colBase += Nsub, reps++) { // projection iterations

			for (cs = 0; cs < Nsub; cs++) cols[cs] = colBase + cs;
			idft_atoms(n, m, samples, Nsub, cols, Nsub*m, exps_buffer, outcore_A);
			//get outcore_A from A
			/*for (i = 0; i < m; i++) {
				for (j = 0; j < Nsub; j++)
					outcore_A[i*Nsub + j] = A[i*n + cols[j]];
			}*/

			// project the residual onto outcore_A' by evaluating outcore_A' * r
			cblas_cgemv(CblasRowMajor, CblasConjTrans, m, Nsub, (void *)&alpha, (void *)outcore_A, Nsub,
				(void *)r, 1, (void *)&beta, (void *)dots, 1);

			// locate the maximum in dots
			lcpos = (int)cblas_icamax(Nsub, dots, 1);

			local_max_abs = sqrtf(dots[lcpos].real*dots[lcpos].real + dots[lcpos].imag*dots[lcpos].imag);
			if (local_max_abs > max_abs) {
				max_abs = local_max_abs;
				cpos = lcpos + colBase;
			}


		} // projection iterations

  	    // add the identified index to the support
		support[coeffs] = cpos;
		coeffs++;

		// generate the columns 
		idft_atoms(n, m, samples, coeffs, support, coeffs*m, exps_buffer, ASubI);
		//get ASubI from A
		/*for (i = 0; i < m; i++) {
			for (j = 0; j < coeffs; j++)
				ASubI[i*coeffs + j] = A[i*n + support[j]];
		}*/
		// save ASubI into outcore_A to avoid recomputing it after cgels (which overwrites the matrix)
		memcpy((void *)outcore_A, (void *)ASubI, m*coeffs*sizeof(MKL_Complex8));

		// 'N' and m>n :find the least squares solution of an overdetermined system,
		// that is, solve for x the least squares problem : minimize || y - ASubI*x ||

		// copy the signal_mp to LSQ vector for Least SQuares
		memcpy(LSQ, y, m*sizeof(MKL_Complex8));
		// Note that the solution will be stored in the elements of LSQ, from [0] to [coeffs-1]
		LAPACKE_cgels(LAPACK_ROW_MAJOR, 'N', m, coeffs, 1, ASubI, coeffs, LSQ, 1);


		// compute the new residual
		// r = y - ASubI * LSQ;

		// copy the corresponding basis functions to a new column of ASubI (set of selected basis functions)
		// NOTE THAT THIS HAS TO BE DONE AGAIN, because LAPCKE_cgels() writes over the matrix!
		// this is avoided by using outcore_A instead of ASubI,. remebring that the matrix has been saved when it was first computed
		// generate the columns 
		// idft_atoms(n, m, samples, coeffs, support, coeffs*m, exps_buffer, ASubI);
		//get ASubI from A
		/*for (i = 0; i < m; i++) {
			for (j = 0; j < coeffs; j++)
				ASubI[i*coeffs + j] = A[i*n + support[j]];
		}*/

		// r = ASubI * LSQ
		//cblas_cgemv(CblasRowMajor, CblasNoTrans, m, coeffs, (void *)&alpha, (void *)ASubI, coeffs,
		//	(void *)LSQ, 1, (void *)&beta, (void *)r, 1);
		// NOTE HOW outcore_A is used instead of ASubI
		cblas_cgemv(CblasRowMajor, CblasNoTrans, m, coeffs, (void *)&alpha, (void *)outcore_A, coeffs,
			(void *)LSQ, 1, (void *)&beta, (void *)r, 1);

		// r = y - r
		vcSub(m, y, r, r);

		numIts++;
	}

	// copy non-zero coefficients to valuesOut
	memset(valuesOut, 0, maxc*sizeof(MKL_Complex8));
	memcpy(valuesOut, LSQ, coeffs*sizeof(MKL_Complex8));
	// label unexisting support elements if coeffs < maxc
	for (i = coeffs; i < maxc; i++) {
		support[i] = -1;
	}
	// free memory
	mkl_free(ASubI);
	mkl_free(LSQ);
	mkl_free(r);
	mkl_free(dots);
	mkl_free(cols);
	mkl_free(exps_buffer);
	mkl_free(outcore_A);

	return numIts;
}
