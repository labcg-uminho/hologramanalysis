#include <string.h>
#include <math.h>
#include <omp.h>

#include "dftmtx.h"

/* idftmtx is equivalent to its MatLab homonymous
* it will return in *A the inverse 1D dft transformation matrix for:
* n dimensional signals, meaning the matrix will have n columns
* m points in the signal_mp space doimain, meaning the matrix will have m rows
* the indices of these points (i.e., which fourier inverse basis functions to evaluate)
* are given in *ndx, a m-element vector, with each element being an integer in the range 0 to n-1
* note that the storage space in *A = m*n*sizeof(MKL_Complex8) must be previously allocated
*/

int idftmtx(MKL_Complex8 *A, int *ndx, int n, int m) {
	DFTI_DESCRIPTOR_HANDLE hand = 0;
	/* Execution status */
	MKL_LONG status = 0;


	/* The descriptor data structure, when created, contains information about:
	* the length and domain of the FFT to be computed,
	* as well as the setting of several configuration parameters.
	* Default settings for some of these parameters are:
	* * Scale factor: none
	* * Number of data sets: one
	* * Data storage: contiguous
	* * Placement of results: in-place (the computed result overwrites the input data)
	*/
	status = DftiCreateDescriptor(&hand, DFTI_SINGLE, DFTI_COMPLEX, 1, (MKL_LONG)n);
	if (0 != status) return 0;

	status = DftiSetValue(hand, DFTI_PLACEMENT, DFTI_INPLACE);
	if (0 != status) return 0;

	status = DftiSetValue(hand, DFTI_COMPLEX_STORAGE, DFTI_COMPLEX_COMPLEX);
	if (0 != status) return 0;

	// performing m transforms
	status = DftiSetValue(hand, DFTI_NUMBER_OF_TRANSFORMS, (MKL_LONG)m);
	if (0 != status) return 0;

	// the m signals are stored in A, and they are n elements apart
	status = DftiSetValue(hand, DFTI_INPUT_DISTANCE, (MKL_LONG)n);
	if (0 != status) return 0;

	// the m signals are stored in A, and they are n elements apart
	status = DftiSetValue(hand, DFTI_BACKWARD_SCALE, 1.0f / ((float)n));
	if (0 != status) return 0;

	status = DftiCommitDescriptor(hand);
	if (0 != status) return 0;

	// initialize A
	memset((void *)A, 0, n*m*sizeof(MKL_Complex8));
	int col;

	for (col = 0; col<m; col++) {
		A[col*n + ndx[col]].real = 1.f;
	}

	// Compute backward transform
	status = DftiComputeBackward(hand, A);
	if (0 != status) return 0;

	DftiFreeDescriptor(&hand);
	return 1;
}

//int idft_atoms(int N, int n_rows, int *rows, int n_cols, int *cols, int n_results, MKL_Complex8 *exps, MKL_Complex8 *results) {
//	MKL_Complex8 *aux;
//	int row, col, ele;
//	const float N_reciprocal = 1. / ((double)N);
//	const float angular_delta = 2.f * MY_PIf * N_reciprocal;
//
//	aux = (MKL_Complex8 *)mkl_malloc(N*n_rows*sizeof(MKL_Complex8), 64);
//	idftmtx(aux, rows, N, n_rows);
//	for (row = 0; row < n_rows; row++) {
//		for (col = 0; col < n_cols; col++)
//			results[row*n_cols + col] = aux[row*N + cols[col]];
//	}
//	mkl_free(aux);
//
//	return 1;
//}

//int idft_atoms(int N, int n_rows, int *rows, int n_cols, int *cols, int n_results, MKL_Complex8 *exps, MKL_Complex8 *results) {
//	MKL_Complex8 *aux;
//	int row, col, ele;
//	const float N_reciprocal = 1.f / ((float)N);
//	const float angle_step = ((float)(2. * MY_PI)) * N_reciprocal;
//
//	if (n_results != (n_rows*n_cols)) return 0;
//	if ((n_rows > N) || (n_cols > N)) return 0;
//
//	for (aux = exps, row = 0; row < n_rows; row++) {
//		for (col = 0; col < n_cols; aux++, col++) {
//			aux->real = 0.f;
//			aux->imag = angle_step * ((float)(cols[col] * rows[row]));
//		}
//	}
//	vcExp(n_results, exps, results);
//
//	for (row = 0; row < n_rows; row++) {
//		for (col = 0; col < n_cols; col++) {
//			exps[row*n_cols + col].real = N_reciprocal;
//			exps[row*n_cols + col].imag = 0.f;
//		}
//	}
//	vcMul(n_results ,results, exps, results);
//
//	return 1;
//}

//int idft_atoms(int N, int n_rows, int *rows, int n_cols, int *cols, int n_results, MKL_Complex8 *exps, MKL_Complex8 *results) {
//	MKL_Complex8 *aux;
//	int row, col, ele;
//	const double N_reciprocal = 1. / ((double)N);
//	const double angular_delta = 2. * MY_PI * N_reciprocal;
//
//	if (n_results != (n_rows*n_cols)) return 0;
//	if ((n_rows > N) || (n_cols > N)) return 0;
//
//	for (aux = results, row = 0; row < n_rows; row++) {
//		for (col = 0; col < n_cols; aux++, col++) {
//			double angle = angular_delta * ((double)(cols[col] * rows[row]));
//			aux->real = (float)(cos(angle) * N_reciprocal);
//			aux->imag = (float)(sin(angle) * N_reciprocal);
//		}
//	}
//
//	return 1;
//}

int idft_atoms(int N, int n_rows, int *rows, int n_cols, int *cols, int n_results, MKL_Complex8 *exps, MKL_Complex8 *results) {
	MKL_Complex8 *aux;
	int row, col;
	const float N_reciprocal = 1.f / ((float)N);
	const float angular_delta = 2.f * MY_PIf * N_reciprocal;

	if (n_results != (n_rows*n_cols)) return 0;
	if ((n_rows > N) || (n_cols > N)) return 0;

	aux = results;
	// in order to use OpenMP uncomment the next line
#pragma omp parallel for private (col)
	for (row = 0; row < n_rows; row++) {
		for (col = 0; col < n_cols; col++) {
			const float angle = angular_delta * ((float)(cols[col] * rows[row]));
			(aux + row*n_cols + col)->real = cosf(angle) * N_reciprocal;
			(aux + row*n_cols + col)->imag = sinf(angle) * N_reciprocal;
		}
	}

	return 1;
}
