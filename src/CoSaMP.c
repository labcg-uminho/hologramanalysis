#include "CoSaMP.h"
#include "dftmtx.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "ipps.h"

#define MAX_ITERS 32

// Returns 0 if some error occurs
// returns number of completed iterations otherwise

int CoSaMP(int maxc, int *samples, MKL_Complex8 *y, int *support, MKL_Complex8 *coeffs, int n, int m) {
  int i, numIts, *cols = 0;
  MKL_Complex8 *A = 0;

  cols = (int *)mkl_malloc(n * sizeof(int), 64);
  if (0 == cols) fprintf(stderr , "CoSaMP: could not malloc cols");

  for (i = 0; i < n; i++) cols[i] = i;

  numIts = CoSaMP_supp (maxc, y, coeffs, support, n, n, cols, m, samples);

  mkl_free(cols);

  return numIts;
}

int CoSaMP_supp (int maxc, MKL_Complex8 *y, MKL_Complex8 *valuesOut, int *support, int N, int n_cols, int *cols,
                        int m, int *samples) {
  int i, numIts, coeffs, *ndx = 0, *T = 0, *prev_support = 0;
  int diverge = 0, progress=0;
  MKL_Complex8 alpha = { 1., 0. }, beta = { 0., 0. }, *dots = 0, *r = 0, *ASubI = 0, *LSQ = 0, *prev_coeff = 0;
  MKL_Complex8 *outcore_A = 0, *exps_buffer = 0;
  float *abs_dots = 0, *largest_abs_dots = 0;
  int *largest_abs_dots_ndx = 0, *buff_abs_dots_ndx = 0;
  int draft = 0; // is this a single iteration searching for a draft support only?
  const int Nsub = maxc, Nreps = n_cols / Nsub;
  int reps, colBaseNdx;

  if (n_cols < maxc) {
    fprintf(stderr , "CoSaMP: N (%d) must be larger or equal than k(%d)!Local supports can't be evaluated", N, maxc);
    return 0;
  }
  if (valuesOut == NULL) {
    draft = 1; // search for support on a single iteration
  }

  // dots: n*1 vector to store the n dot products between A_transpose and the
  // residual
  dots = (MKL_Complex8 *)mkl_malloc(Nsub * sizeof(MKL_Complex8), 64);
  if (dots == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing the dot products");
    return 0;
  }
  // abs_dots: 3*maxc*1 vector to store the magnitudes of the above n complex
  // dot products
  abs_dots = (float *)mkl_malloc(3 * maxc * sizeof(float), 64);
  if (abs_dots == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing the abs dot products");
    return 0;
  }

  // r: m*1 vector to store the residual
  r = (MKL_Complex8 *)mkl_malloc(m * sizeof(MKL_Complex8), 64);
  if (r == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing the residual");
    return 0;
  }

  // LSQ: m*1  vector to store the least squares (LSQ) result
  // (note that m elements are required -- even though there are at most 2*maxc
  // coefficients recovered this is because LSQ is also the input buffer for the
  // LSQ routine and because this routine returns additional info on the
  // remaining elements of LSQ ...
  LSQ = (MKL_Complex8 *)mkl_malloc(m * sizeof(MKL_Complex8), 64);
  if (LSQ == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing LSQ results");
    return 0;
  }

  // ASubI: m rows x 3*maxc columns matrix with the candidate basis functions
  ASubI = (MKL_Complex8 *)mkl_malloc(m * 3 * maxc * sizeof(MKL_Complex8), 64);
  if (ASubI == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing the candidate "
                "basis functions");
    return 0;
  }

  // T: 3*maxc vector holding the indexes candidates at each iteration
  T = (int *)mkl_malloc(3 * maxc * sizeof(int), 64);
  if (T == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing the candidate indexes");
    return 0;
  }

  // exps buffer - buffer vector to the exponents within idft_atoms
  exps_buffer = (MKL_Complex8 *)mkl_malloc(3 * maxc * m * sizeof(MKL_Complex8), 64);
  if (exps_buffer == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing exps_buffer for idft_atoms");
    return 0;
  }
  // incore_A used within the outcore version to hold the NSub columns (m rows
  // each)
  outcore_A = (MKL_Complex8 *)mkl_malloc(m * Nsub * sizeof(MKL_Complex8), 64);
  if (outcore_A == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing the (outcore) "
                "selected basis functions");
    return 0;
  }

  // PREVIOUS Support: for rollback see below
  prev_support = (int *)mkl_malloc(maxc * sizeof(int), 64);
  if (prev_support == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing the previous "
                "iteration support");
    return 0;
  }
  // PREVIOUS Coefficients: for rollback see below
  prev_coeff = (MKL_Complex8 *)mkl_malloc(maxc * sizeof(MKL_Complex8), 64);
  if (prev_coeff == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing the previous "
                "iteration coefficients");
    return 0;
  }

  const int max_new_coeffs_per_iteration = (n_cols >= (2 * maxc) ? 2 * maxc : n_cols);
  // Support: vector holding sorted indexes
  ndx = (int *)mkl_malloc((max_new_coeffs_per_iteration + Nsub) * sizeof(int), 64);
  if (ndx == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing the sorted indexes");
    return 0;
  }
  largest_abs_dots = (float *)mkl_malloc((max_new_coeffs_per_iteration + Nsub) * sizeof(float), 64);
  if (largest_abs_dots == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing the largest abs dots");
    return 0;
  }
  largest_abs_dots_ndx = (int *)mkl_malloc((max_new_coeffs_per_iteration + Nsub) * sizeof(int), 64);
  if (largest_abs_dots_ndx == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for storing the largest abs "
                "dots indices");
    return 0;
  }
  buff_abs_dots_ndx = (int *)mkl_malloc((max_new_coeffs_per_iteration + Nsub) * sizeof(int), 64);
  if (buff_abs_dots_ndx == 0) {
    fprintf(stderr , "CoSaMP: could not alloc memory for temporarily buffer the "
                "largest abs dots indices");
    return 0;
  }

  numIts = 0;
  // initialize residual with the signal_mp
  memcpy(r, y, m * sizeof(MKL_Complex8));

  // for computing the normalized length of the residual
  const float Inv_sqr_m = 1.f / sqrtf((float)m);

  // numerical instabilities (maybe due to the fact that signle precision is
  // being used) deviate the residual's norm from zero therefore we add an
  // additional termination condition: if the residual_norm is larger than the
  // previous residual_norm then rollback
  float r_norm = 1.e20f, prev_r_norm;
  const float Inv_y_norm = sqrtf((float)m) / cblas_scnrm2(m, y, 1);

  do {

    // backup the results from the previous iteration
    // in case it has to rollback because the residual_norm increased
    if (numIts) {
      memcpy(prev_support, support, maxc * sizeof(int));
      memcpy(prev_coeff, valuesOut, maxc * sizeof(MKL_Complex8));
    }

	// the following code segment (Residual projection) is use dto identify the 2*maxc most relevant columns 
	// (within those in cols[]) to represent the residual
	// it is executed if and only if the number of candidate columns (as given by n_cols) is larger than 2*maxc

	if (n_cols > 2 * maxc) {
		coeffs = 0;                                                                // no coeffs identified up to now
		for (colBaseNdx = 0, reps = 0; reps < Nreps; colBaseNdx += Nsub, reps++) { // projection iterations

			// generate the IDFT for the Nsub columns identified from cols[colBaseNdx]
			idft_atoms(N, m, samples, Nsub, &cols[colBaseNdx], Nsub * m, exps_buffer, outcore_A);

			// project the residual onto A' by evaluating A' * r
			cblas_cgemv(CblasRowMajor, CblasConjTrans, m, Nsub, (void *)&alpha, (void *)outcore_A, Nsub, (void *)r, 1,
				(void *)&beta, (void *)dots, 1);

			// sort the absolut values of the projections
			vcAbs(Nsub, dots, abs_dots);

			// We want at any instant to identify the max_coeffs largest abs_dots.
			// largest_abs_dots contains the lasrgest ones identified up to now
			// -> Add these Nsub to them
			memcpy((void *)(largest_abs_dots + coeffs), (void *)abs_dots, Nsub * sizeof(float));
			// largest_abs_dots_ndx contains the respective indexes within cols[]
			// NOTE: these are the local indexes within cols[] (going from 0 to
			// n_cols-1)
			//  and not the globals (from 0 to N-1)
			for (i = 0; i < Nsub; i++) largest_abs_dots_ndx[i + coeffs] = colBaseNdx + i;
			// buffer these indexes, because we will need that
			memcpy((void *)buff_abs_dots_ndx, (void *)largest_abs_dots_ndx, (coeffs + Nsub) * sizeof(int));

			// Now sort largest_abs_dots
			ippsSortIndexDescend_32f_I((Ipp32f *)largest_abs_dots, ndx, coeffs + Nsub);
			// update the number of coeffs in largest_abs_dots
			coeffs = (coeffs + Nsub < max_new_coeffs_per_iteration ? coeffs + Nsub : max_new_coeffs_per_iteration);

			// update the initial coeffs elements of largest_abs_dots_ndx, according
			// to the reshuffle in ndx
			for (i = 0; i < coeffs; i++) largest_abs_dots_ndx[i] = buff_abs_dots_ndx[ndx[i]];

		} // Nsub reps

		// T will hold the candidate indexes for this iteration
		// The new set of candidates comes from the projection (largest dot
		// products)
		//
		// In general these are 2*maxc
		// but OBVIOUSLY the number of selected coefficients CANNOT be LARGER than
		// the number of columns in A (n) Note once gain that indexes are
		// local(index cols[])
		memcpy(T, largest_abs_dots_ndx, coeffs * sizeof(int));

		// now convert the indexes in T (there are coeffs) from LOCAL to GLOBAL
		for (i = 0; i < coeffs; i++) T[i] = cols[T[i]];
	}  // code that selects 2*maxc cols among the n_cols identified in cols[]
	else
	{   // what happens if n_cols <= 2*maxc
		coeffs = n_cols;
		memcpy(T, cols, coeffs * sizeof(int));
	}
    // the remaining maxc candidates are the support of the previous iteration
    // if this is the first iteration no such thing exists
    // note that the maxc coeffs from the previous iteration can only be added
    // if n_cols >= 3*maxc Note also that the indexes in support are global,
    // this is way the previous conversion was performed!
    if ((numIts) && (n_cols >= 3 * maxc)) {
      memcpy(T + coeffs, support, maxc * sizeof(int));
      coeffs = 3 * maxc;
    }
    // sort T
    ippsSortAscend_32s_I((Ipp32s *)T, coeffs);

    //// copy the corresponding basis functions to a new column of ASubI (set of
    /// selected basis functions)
    // for (i = 0; i < m; i++) {
    //	for (j = 0; j < coeffs; j++)
    //		ASubI[i*coeffs + j] = A[i*n + T[j]];
    //}
    // generate the IDFT for the coeffs columns identified from T[]
    idft_atoms(N, m, samples, coeffs, T, coeffs * m, exps_buffer, ASubI);

    // 'N' and m>n :find the least squares solution of an overdetermined system,
    // that is, solve for x the least squares problem : minimize || y - ASubI*x
    // ||

    // copy the signal_mp to LSQ vector for Least SQuares
    memcpy(LSQ, y, m * sizeof(MKL_Complex8));
    // Note that the solution will be stored in the elements of LSQ, from [0] to
    // [coeffs-1] LAPACKE_cgels returns 0 on success
    if (LAPACKE_cgels(LAPACK_ROW_MAJOR, 'N', m, coeffs, 1, ASubI, coeffs, LSQ, 1)) {
      fprintf(stderr , "CoSaMP_supp: Least Squares couldn't find a solution");
      return 0;
    }

    // Remember that each element of LSQ (from [0] to [coeffs-1]) contains the
    // coefficient value for the corresponding basis function on the current set
    // of candidates These are 'coeffs' values, whose indexes are stored in T.
    // in order to identify the INDEXES of the maxc larger coefficients we will
    // sort them
    vcAbs(coeffs, LSQ, abs_dots);
    ippsSortIndexDescend_32f_I((Ipp32f *)abs_dots, ndx, coeffs);

    // and copy the indexes (as given in T) of the maxc largest ones to support
    // also copy the corresponding coefficients values into a vector (maxc
    // elements) for the multiplication below;
    // printf("Iter %d - Recovered freqs:", numIts);

    for (i = 0; i < maxc; i++) {
      support[i] = T[ndx[i]];
    }

    if (!draft && (n_cols > 2*maxc)) {

      for (i = 0; i < maxc; i++) valuesOut[i] = LSQ[ndx[i]];

      // support holds the global indexes which are the solution for THE CURRENT
      // ITERATION

      // compute the new residual
      // r = y - ASubI * LSQ;
      // Computing the residual is only done if iterating
      // i.e., if this is a draft (single iteration, searching for support only)
      // then don't

      // copy the corresponding basis functions to a new column of ASubI (set of
      // selected basis functions) NOTE THAT THIS HAS TO BE DONE AGAIN, because
      // both LAPCKE_cgels() writes over the matrix AND the indexes in support
      // are a subset of those which were on T
      // for (i = 0; i < m; i++) {
      //	for (j = 0; j < maxc; j++)
      //		ASubI[i*maxc + j] = A[i*n + support[j]];
      //}
      // generate the IDFT for the coeffs columns identified from T[]
      idft_atoms(N, m, samples, maxc, support, maxc * m, exps_buffer, ASubI);

	  // r = ASubI * LSQ
      cblas_cgemv(CblasRowMajor, CblasNoTrans, m, maxc, (void *)&alpha, (void *)ASubI, maxc, (void *)valuesOut, 1,
                  (void *)&beta, (void *)r, 1);

      // r = y - r
      vcSub(m, y, r, r);

      prev_r_norm = r_norm;
      r_norm = cblas_scnrm2(m, r, 1) * Inv_sqr_m;
	  r_norm *= Inv_y_norm;  // this is ||r|| / ||y||

      diverge = (r_norm > prev_r_norm);
	  progress = ((prev_r_norm - r_norm) > 1e-1);

    } // if (!draft)
	else if (!draft && (n_cols <= 2 * maxc)) {
		for (i = 0; i < maxc; i++) valuesOut[i] = LSQ[ndx[i]];
		progress = 0;
	}
    numIts++;

  } while (!draft && (numIts < MAX_ITERS) && (r_norm > 1.e-1) && !diverge && progress);

  // if the residual norm increased, then roll back
  if (!draft && diverge) {
    memcpy(support, prev_support, maxc * sizeof(int));
    memcpy(valuesOut, prev_coeff, maxc * sizeof(MKL_Complex8));
  }

  // free memory
  mkl_free(ndx);
  mkl_free(T);
  mkl_free(ASubI);
  mkl_free(LSQ);
  mkl_free(r);
  mkl_free(abs_dots);
  mkl_free(dots);
  mkl_free(prev_coeff);
  mkl_free(prev_support);
  mkl_free(largest_abs_dots);
  mkl_free(largest_abs_dots_ndx);
  mkl_free(buff_abs_dots_ndx);

  // Missing free generating memory leak
  mkl_free(exps_buffer);
  mkl_free(outcore_A);

 return numIts;
}
