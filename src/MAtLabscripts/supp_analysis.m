function   [same_location] = supp_analysis (suppHT, suppMP)
  % the 2 k values below might be different 
  % because the HT function might increase k for equal amplitude coefficients
  kHT = size(suppHT,1);
  kMP = size(suppMP,1);
  
  same_location=0;
  MPndx=1;
  HTndx=1;
  
  % note both supports (HT and MP) are assumed to be sorted in ascending order of indexes
  while (MPndx <= kMP)
    target = suppMP(MPndx);
    
    % locate larger or equal in HT
    while ((HTndx<=kHT) && (target > suppHT(HTndx)))
      HTndx++;
    endwhile
    
    % if equal increase same_location
    if ((HTndx<=kHT) && (target == suppHT(HTndx)))
      same_location++;
    endif
    
    % proceed to next MP index
    MPndx++;
  endwhile

endfunction