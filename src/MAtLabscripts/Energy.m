function energy = Energy (x)
%Energy (x) Signal Energy
%  dot product. Note that if x is complex then this is x * conjugate(x)
 energy = dot(x,x);
%x2 = x .* x;
%energy = sum(x2);
end
