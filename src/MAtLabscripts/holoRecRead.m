function [N, m, k, alg, holofileName, support, nonZeroCoeffs]  =  holoRecRead (fname)
  f= fopen (fname, "r");
  if (f==-1)
    printf ("holoRecRead: error opening %s\n", fname);
    return;
  endif  
  N = fread (f, 1, "int32");
  m = fread (f, 1, "int32");
  k = fread (f, 1, "int32");
  strLen = fread (f, 1, "int32");
  holofileName = fread (f, strLen+1, "*char")';
  strLen = fread (f, 1, "int32");
  alg = fread (f, strLen+1, "*char")';
  support = fread (f, k, "*int32");
  % the support in C is from 0 to N-1: add 1 to comply with MatLab indexing
  support = support+1;
  % these scripts expect column vectors. Transpose support if rows==1
  if (size(support,1)==1)
    support=support.';
  endif
  re_part = fread (f, k, "single");
  im_part = fread (f, k, "single");
  nonZeroCoeffs = re_part + i * im_part;
  % these scripts expect column vectors. Transpose if rows==1
  if (size(nonZeroCoeffs,1)==1)
    nonZeroCoeffs=nonZeroCoeffs.';
  endif
  fclose (f);
endfunction
  