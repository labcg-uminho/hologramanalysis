function [signal, Nhogels] =  holoRead (fname)
  f= fopen (fname, "r");
  if (f==-1)
    printf ("holoRead: error opening %s\n", fname);
    return;
  endif
  holoSize = fread (f, 2, "int32");
  Nhogels = holoSize(1) * holoSize(2);
  signal = fread (f, Nhogels, "double");
  fclose (f);
endfunction
  