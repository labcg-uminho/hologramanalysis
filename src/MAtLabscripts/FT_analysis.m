function [signalHT, signalFT, signalHTFT, supp, k] = FT_analysis (signal, k, signalFT)

  if (!exist("signalFT", "var"))
    signalFT = fft(signal);
  endif
  
  % hard threshold
  [signalHTFT,supp, k] = HT_number(signalFT, k);
  
  % get signalHT
  signalHT = ifft(signalHTFT);
 endfunction