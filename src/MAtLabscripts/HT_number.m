function [x_ht, supp, k] = HT_number (x, k )
%HT_number( x, tol ) Hard threshold based on the sparseness numer
%maintain only the k largest coefficient of x
N = max(size(x));
if k>=N
    x_ht = x;
    supp=[1:N];
    return;
end
if size(x,1) < size(x,2) 
    x = x';
    transp = true;
else
    transp = false;
end
absx = abs(x);
%[[1:N]' absx]
xs = sort(absx,1,'descend');

while ((k < N) && (xs(k) == xs(k+1)))
  k = k+1;
endwhile

if k>=N
    x_ht = x;
    supp=[1:N];
    return;
end

cut = xs(k+1);
ii = (absx <= cut);
xt = x;
xt(ii) = 0;

% compute support only if required
if (isargout(2))
  supp = zeros(k,1);
  n = size(x,1);
  ocnt = 1;
  scnt = 1;
  while ((ocnt<=n) && (scnt<=k))
    if (ii(ocnt)==0)
      supp(scnt) = ocnt;
      scnt++;
    endif
    ocnt++;
  endwhile
endif

if transp
    x_ht=xt';
    if (isargout(2))
      supp = supp';
    endif
else
    x_ht = xt;
end

end

