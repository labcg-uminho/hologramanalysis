function FT_analysis_plot (signal, signalHT, signalMP, k, signalFT, signalHTFT, signalMPFT)

  N = size(signal,1);
  f_range = [0: N-1];
 %plot original and hard thresholded coefficients in representation basis
  figure('Name', "Signal Recovery Analysis");
  subplot(2,2,1)
  xlim ([f_range(1) f_range(N)]);
  ylim ([min([min(signal) min(signalHT)])-0.1 max([max(signal) max(signalHT)])+0.1]);
  hold on;
  plot(f_range, signal, "b;original;")
  plot(f_range, signalHT, "r;hard thresholded;")
  hold off;
  grid off;
  xlabel('Basis Index');
  ylabel('Amplitude');
  title(sprintf('Hard Thresholded, %d coefficients in %d',k, N));
  
  
  % plot spectral power
  spectral_xlim = min([2*k N]);
  if (!exist("signalFT", "var"))
    signalFT = fft(signal);
  endif

  if (!exist("signalHTFT", "var"))
    signalHTFT = fft(signalHT);
  endif
  
  sPowerFT = signalFT .* conj(signalFT);
  sPowerHTFT = signalHTFT .* conj(signalHTFT);

  sPowerFT = sort(sPowerFT, 'descend');
  sPowerHTFT = sort(sPowerHTFT, 'descend');

  subplot(2,2,2)
  xlim ([f_range(1) f_range(spectral_xlim)]);
  ylim ([max([min(sPowerFT(1:spectral_xlim)) min(sPowerHTFT(1:spectral_xlim)) 1e-3]) max([max(sPowerFT(1:spectral_xlim)) max(sPowerHTFT(1:spectral_xlim))])+0.1]);
  hold on;
  semilogy(f_range(1:spectral_xlim), sPowerFT(1:spectral_xlim), "b;original;")
  semilogy (f_range(1:spectral_xlim), sPowerHTFT(1:spectral_xlim), "r;hard thresholded;")
  hold off;
  grid off;
  xlabel('Sorted Indexes');
  ylabel('Spectral Energy');
  title(sprintf('Hard Thresholded, %d coefficients in %d',k, N));
 
  %plot original and recovered signals in representation basis
  subplot(2,2,3)
  xlim ([f_range(1) f_range(N)]);
  ylim ([min([min(signal) min(signalMP)])-0.1 max([max(signal) max(signalMP)])+0.1]);
  hold on;
  plot(f_range, signal, "b;original;")
  plot(f_range, signalMP, "r;recovered;")
  hold off;
  grid off;
  xlabel('Basis Index');
  ylabel('Amplitude');
  title(sprintf('Recovered, %d coefficients in %d',k, N));
  
    % plot spectral power
  if (!exist("signalMPFT", "var"))
    signalMPFT = fft(signalMP);
  endif
  
  sPowerMPFT = signalMPFT .* conj(signalMPFT);

  sPowerMPFT = sort(sPowerMPFT, 'descend');

  subplot(2,2,4)
  xlim ([f_range(1) f_range(spectral_xlim)]);
  ylim ([max([min(sPowerFT(1:spectral_xlim)) min(sPowerMPFT(1:spectral_xlim)) 1e-3]) max([max(sPowerFT(1:spectral_xlim)) max(sPowerMPFT(1:spectral_xlim))])+0.1]);
  hold on;
  semilogy(f_range(1:spectral_xlim), sPowerFT(1:spectral_xlim), "b;original;")
  semilogy (f_range(1:spectral_xlim), sPowerMPFT(1:spectral_xlim), "r;recovered;")
  hold off;
  grid off;
  xlabel('Sorted Indexes');
  ylabel('Spectral Energy');
  title(sprintf('Recovered, %d coefficients in %d',k, N));
  
   %plot FTs for comparison purposes
  figure('Name', "Signal Recovery Analysis - Fourier Transforms Magnitudes");
  
  abs_signalFT = abs(fftshift(signalFT));
  abs_signalHTFT = abs(fftshift(signalHTFT));
  abs_signalMPFT = abs(fftshift(signalMPFT));
  
  FT_range = [-N/2: (N-1)/2];
  
  % original signal FT 
  subplot(1,3,1)
  bar(FT_range, abs_signalFT, 0.1)
  xlim ([FT_range(1) FT_range(N)]);
  ylim ([0 max(abs_signalFT)+1]);
  grid off;
  xlabel('Basis Index');
  ylabel('Magnitude');
  title(sprintf('Original Signal Fourier Representation, %d coefficients',N));
 
   % hard thresholded signal FT   
  subplot(1,3,2)
  bar(FT_range, abs_signalHTFT, 0.1)
  xlim ([FT_range(1) FT_range(N)]);
  ylim ([0 max(abs_signalHTFT)+1]);
  grid off;
  xlabel('Basis Index');
  ylabel('Magnitude');
  title(sprintf('HT Signal Fourier Representation, %d in %d coefficients',k, N));

  % recovered signal FT   
  subplot(1,3,3)
  bar(FT_range, abs_signalMPFT, 0.1, "basevalue", 1e-2)
  xlim ([FT_range(1) FT_range(N)]);
  ylim ([1e-2 max(abs_signalMPFT)+1]);
  %set (gca,'yscale','log');
  grid off;
  xlabel('Basis Index');
  ylabel('Magnitude');
  title(sprintf('Recovered Signal Fourier Representation, %d in %d coefficients',k, N));
 endfunction