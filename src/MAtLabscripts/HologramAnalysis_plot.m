function HologramAnalysis_plot (fname)

  close all;
  
  [N, m, k, alg, holofileName, suppMP, nonZeroCoeffs] = holoRecRead (fname);  
  

  [signal, Nholo] =  holoRead (holofileName);
  
  if (N != Nholo)
    printf ("signal size different from saved rec data (%d) amnd holo file (%d)\n", N, Nholo);
    return;
  endif
  
  printf ("Parameters - %s; holofileName: %s; N=%d; m=%d, k=%d\n", alg, holofileName, N, m, k);
  % Hard Thresholding
  [signalHT, signalFT, signalHTFT, suppHT, k] = FT_analysis (signal, k);
  signal_Energy = Energy(signal);
  signalHT_Energy = Energy(signalHT);
  signalHTDiff_Energy = 100. * Energy(signal-signalHT) / signal_Energy;
  printf ("Energy - signal: %.1f; signalHT: %.1f (%.0f%%); signalHT_Diff_Norm: %.0f%% \n", signal_Energy, signalHT_Energy, 100*signalHT_Energy/signal_Energy, signalHTDiff_Energy); 

  signalMPFT = complex(zeros(N, 1), zeros(N,1));
  signalMPFT(suppMP) = nonZeroCoeffs;
  
  signalMP = ifft(signalMPFT);
  
%  printf ("signal:");
%  size(signal)
%  size(signalFT)
%  printf ("signalHT:");
%  size(signalHT)
%  size(suppHT)
%  size(signalHTFT)
%  printf ("signalMP:");
%  size(signalMP)
%  size(suppMP)
%  size(signalMPFT)
%  return;
  signalMP_Energy = Energy(signalMP);
  signalMPDiff_Energy = 100. * Energy(signal-signalMP) / signal_Energy;
  printf ("Energy - signal: %.1f; signalMP: %.1f (%.0f%%); signalMP_Diff_Norm: %.0f%% \n", signal_Energy, signalMP_Energy, 100*signalMP_Energy/signal_Energy, signalMPDiff_Energy); 
  
  suppMP = sort(suppMP,'ascend');
  %[suppHT suppMP]
  [same_location] = supp_analysis (suppHT, suppMP);
  printf ("MP support has %d coefiicients (out of %d) in the same location as HT (%.0f%%) \n", same_location, k, 100*same_location/k); 
  
  % plotting
  FT_analysis_plot(signal, signalHT, signalMP, k, signalFT, signalHTFT, signalMPFT);
  
endfunction