
#pragma once
#include "mkl.h"

int omp(int maxc, int *samples, MKL_Complex8 *y, int *support, MKL_Complex8 *coeffs, int n, int m);
int omp_supp(int maxc, MKL_Complex8 *y, MKL_Complex8 *coeffs, int *support, int n, int m, int *samples);
