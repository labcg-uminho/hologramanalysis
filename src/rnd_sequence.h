

#include "mkl_vsl.h"

/* rnd_sequence (int *r, int n, int m)
* it will return in *r a sorted sequence of m integers uniformly distributed
* between 0 and n-1
*/

int rnd_sequence(int *r, int n, int m);
int sorted_rnd_sequence(int *r, int n, int m);
