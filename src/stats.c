
#include <stdio.h>
#include <mkl.h>
#include <ipps.h>

static int HardThreshold(MKL_Complex8 *s, int N, int k, int *supp);
static float Energy(int N, MKL_Complex8 *s);

int recovery_stats(int N, int m, int k, int *support, MKL_Complex8 *nonZeroCoeffs, MKL_Complex8 *signal_full) {

	// compute % support
	// need the Fourier transform of the HT version
	MKL_Complex8 *signal_HTFT = NULL, *signal_MPFT = NULL, *signal_MP = NULL;
	float Energy_full, EnergyFT_full, Energy_HT, Energy_MP;
	int *HT_supp = NULL;
	int j, i, good_location;

	// alloc storage
	signal_HTFT = (MKL_Complex8 *)mkl_malloc(N*sizeof(MKL_Complex8), 64);
	if (!signal_HTFT) {
		fprintf(stderr, "recovery_stats(): could not allocate memory.\n");
		return 0;
	}
	HT_supp = (int *)mkl_malloc(k*sizeof(int), 64);
	if (!HT_supp) {
		fprintf(stderr, "recovery_stats(): could not allocate memory.\n");
		return 0;
	}

	// original signal energy: NOTE computed over time
	Energy_full = Energy(N, signal_full);

	// 1st compute the Fourier transform of the original signal:

	DFTI_DESCRIPTOR_HANDLE hand = 0;
	/* Execution status */
	MKL_LONG status = 0;
	status = DftiCreateDescriptor(&hand, DFTI_SINGLE, DFTI_COMPLEX, 1, (MKL_LONG)N);
	if (0 != status) return 0;
	status = DftiSetValue(hand, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
	if (0 != status) return 0;
	status = DftiSetValue(hand, DFTI_COMPLEX_STORAGE, DFTI_COMPLEX_COMPLEX);
	if (0 != status) return 0;
	// performing 1 transform
	status = DftiSetValue(hand, DFTI_NUMBER_OF_TRANSFORMS, (MKL_LONG)1);
	if (0 != status) return 0;
	// iFFT normalization constant equal to MatLAB's
	status = DftiSetValue(hand, DFTI_BACKWARD_SCALE, 1.0f / ((float)N));
	if (0 != status) return 0;
	status = DftiCommitDescriptor(hand);
	if (0 != status) return 0;

	// Compute forward transform
	status = DftiComputeForward(hand, signal_full, signal_HTFT);
	if (0 != status) return 0;

	// compute signal energy over the FT instead of time: just as a sanity check - comment after verification
	// remember that since it is eval over FT it has to be divided by the iFFT normaliztion constant 1/N 
	//EnergyFT_full = Energy(N, signal_HTFT) / ((float) N);


	// hard threshold HTFT
	if (!HardThreshold(signal_HTFT, N, k, HT_supp)) return 0;

	// compute % support without assuming sorted supports
	for (good_location = 0, i = 0; i < k; i++) { // iterate over HT_supp
		const int target_ndx = HT_supp[i];
		for (j = 0; (j < k) && (support[j] != target_ndx); j++); // iterate over support (recovered signal)
		if (j < k) good_location++;
	}

	// HT signal energy: NOTE computed over FT
	// remember that since it is eval over FT it has to be divided by the iFFT normaliztion constant 1/N 
	Energy_HT = Energy(N, signal_HTFT) / ((float)N);

	// MP signal energy: NOTE computed only over the k nonZeroCoeffs
	// remember that since it is eval over FT it has to be divided by the iFFT normaliztion constant 1/N 
	Energy_MP = Energy(k, nonZeroCoeffs) / ((float)N);

	// no more FFTs: free the FFT descriptor
	DftiFreeDescriptor(&hand);


	fprintf(stdout, "Energy - signal: %.1f; signalHT: %.1f (%.0f%%);\n", Energy_full, Energy_HT, 100. * Energy_HT / Energy_full);
	//fprintf(stdout, "Energy - signalFT: %.1f\n", EnergyFT_full);
	fprintf(stdout, "Energy - signal: %.1f; signalMP: %.1f (%.0f%%);\n", Energy_full, Energy_MP, 100. * Energy_MP / Energy_full);
	fprintf(stdout, "MP support has %d coefiicients (out of %d) in the same location as HT (%.0f%%)\n", good_location, k, 100. * ((float)good_location) / ((float)k));

	mkl_free(signal_HTFT);
	mkl_free(HT_supp);

	return 1;
}

static int HardThreshold(MKL_Complex8 *s, int N, int k, int *supp) {
	float *s_abs = NULL;
	int *ndx = NULL, j;

	// alloc storage
	s_abs = (float *)mkl_malloc(N*sizeof(float), 64);
	if (!s_abs) {
		fprintf(stderr, "HardThreshold(): could not allocate memory.\n");
		return 0;
	}
	ndx = (int *)mkl_malloc(N*sizeof(int), 64);
	if (!ndx) {
		fprintf(stderr, "HardThreshold(): could not allocate memory.\n");
		return 0;
	}

	vcAbs(N, s, s_abs);
	ippsSortIndexDescend_32f_I((Ipp32f *)s_abs, ndx, N);

	// save support
	for (j = 0; j < k; j++) supp[j] = ndx[j];
	// reset appropriate elements of s
	for (j = k; j < N; j++) s[ndx[j]].real = s[ndx[j]].imag = 0.;

	mkl_free(s_abs);
	mkl_free(ndx);
	return 1;
}

static float Energy(int N, MKL_Complex8 *s) {
	float res;

	cblas_cdotc_sub(N, (void *)s, 1, (void *)s, 1, (void *)&res);
	return res;
}
