#pragma once

int holoread(char *, MKL_Complex8 **);
int save_exp_data(char *holofileName, int N, int m, int k, char * alg, int *support, MKL_Complex8 *nonZeroCoeffs);
